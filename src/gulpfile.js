'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

var sass_path = './css/**/*.scss';
 
gulp.task('sass', function () {
	return gulp.src(sass_path)
		.pipe(sourcemaps.init())
		.pipe(sass({
			outputStyle: 'nested'
		}).on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['> 1%', 'last 2 versions', 'ie 9'],
			cascade: false
		}))
		.pipe(sourcemaps.write('./', {
			includeContent: false,
			sourceRoot: '../src/css',
		}))
		.pipe(gulp.dest('../css'));
});

gulp.task('watch', function () {
	gulp.watch(sass_path, ['sass']);
});