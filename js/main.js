(function($){
	'use strict';
	var _m_name = 'tabbed-list',
		_m_selector = '.' + _m_name,
		_events = {
			'click [m]__link' : 'showTab'
		};

	function moduleSelector(_sel){
		return _sel.replace(/\[m\]/g, _m_selector);
	}

	var Module = function(el){
		this.init(el);
	};

	Module.prototype.init = function(el){
		this.$el = $(el);
		this.$links = $(moduleSelector('[m]__link'));
		this.$panels = $(moduleSelector('[m]__panel'));
		this.loaded_panels = [];
		this.base_url = this.$el.data(_m_name + '-url');
		this.baseUrlTemplate = _.template(this.base_url);
		this.template = _.template($(this.$el.data(_m_name + '-template')).html());
		this.setupEvents();
		this.$el.addClass(_m_name + '--ready');

		this.$links.first().click();
	};

	Module.prototype.setupEvents = function(){
		var self = this;
		$.each(_events, function(_ev, _method_id) {
			var _ev_data = _ev.split(' '),
				_event = _ev_data[0],
				_selector = (function(){
					_ev_data.shift();
					if(_ev_data) {
						return moduleSelector(_ev_data.join(' '));
					}
				})(),
				_method = typeof self[_method_id] == 'function' 
					? self[_method_id]
					: null;
			if(_method){
				self.$el.on(_event, _selector, function(e){
					 _method.apply(self, [e]);
				});
			}

		});
		
	};

	Module.prototype.showTab = function(e){
		var $l = $(e.currentTarget),
			$t = $($l.attr('href'));
		$t.addClass('visible');
		$l.addClass('active');
		this.$links.not($l).removeClass('active');
		this.$panels.not($t).removeClass('visible');
		this.load($t.data(_m_name + '-data'), $t);
	};

	Module.prototype.load = function(url_data, $container){
		var self = this,
			url = this.baseUrlTemplate(url_data),
			id = $container.attr('id');

		if(this.loaded_panels[id]){
			return false;
		}

		$.ajax({
			url: url,
			dataType: 'jsonp',
		}).done(function(data){
			self.loaded_panels[id] = true;
			if(data.response && data.response.status=='ok'){
				self.render(data.response, $container);
			} else {

			}
		});

	};

	Module.prototype.render = function(data, $container){
		var base_data = $container.data('tabbed-content-data') || {},
			content_data = _.extend(base_data, data);
		 $container.html( this.template(content_data) );

	};

	$(_m_selector).each(function(){
		new Module(this);
	});


})(window.jQuery);